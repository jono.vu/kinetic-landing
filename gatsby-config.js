/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

module.exports = {
  plugins: [
    `gatsby-plugin-sass`,
    {
      resolve: "gatsby-source-prismic-graphql",
      options: {
        repositoryName: "kinetic-landing",
        accessToken:
          "MC5Ya05vR1JNQUFDSUFyQXZm.TQXvv70xfwMMZe-_ve-_ve-_ve-_vRzvv73vv73vv73vv70WYCsdY3fvv73vv71RYO-_ve-_vQpTFQ",
      },
    },
  ],
}
