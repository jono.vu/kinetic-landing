import React, { useState } from "react"

import { useWindowDimensions } from "../utils/useWindowDimensions"

import Layout from "./layout/index"
import MobileMenu from "./MobileMenu"
import Navigator from "./Navigator"
import Surface from "./Surface"

const Landing = () => {
  const { width } = useWindowDimensions()
  const isMobile = width < 660

  const [isDragging, setIsDragging] = useState(false)

  const [navigatorTransform, setNavigatorTransform] = useState({
    x: 500,
    y: 600,
  })

  const handleMouseMove = e => {
    if (isDragging)
      setNavigatorTransform({
        x: e.nativeEvent.clientX,
        y: e.nativeEvent.clientY,
      })
  }

  const [previewed, setPreviewed] = useState(0)

  const [activeSection, setActiveSection] = useState(0)

  return (
    <>
      {!isMobile ? (
        <Layout activeSection={activeSection} isDragging={isDragging}>
          <>
            <Navigator
              isDragging={isDragging}
              setIsDragging={e => setIsDragging(e)}
              navigatorTransform={navigatorTransform}
              previewed={previewed}
              setPreviewed={e => setPreviewed(e)}
              activeSection={activeSection}
              setActiveSection={e => setActiveSection(e)}
            />
            <Surface
              isDragging={isDragging}
              setIsDragging={e => setIsDragging(e)}
              handleMouseMove={e => handleMouseMove(e)}
              previewed={previewed}
              setPreviewed={e => setPreviewed(e)}
              activeSection={activeSection}
              setActiveSection={e => setActiveSection(e)}
            />
          </>
        </Layout>
      ) : (
        <Layout activeSection={activeSection} isMobile={isMobile}>
          <MobileMenu
            activeSection={activeSection}
            setActiveSection={e => setActiveSection(e)}
          />
          <Surface
            activeSection={activeSection}
            setActiveSection={e => setActiveSection(e)}
            isMobile={isMobile}
          />
        </Layout>
      )}
    </>
  )
}

export default Landing
