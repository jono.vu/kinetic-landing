import React from "react"

import { variables } from "../../utils/variables"

import { useWindowDimensions } from "../../utils/useWindowDimensions"

const Bg = props => {
  const isCondensed = props.isCondensed

  const section = props.section
  const totalSections = props.totalSections
  const number = props.number

  const color = section.color

  const isDragging = props.isDragging

  const previewed = props.previewed
  const setPreviewed = props.setPreviewed

  const activeSection = props.activeSection
  const setActiveSection = props.setActiveSection

  const { height, width } = useWindowDimensions()

  const angle = variables.panels.angle
  const baseWidth = !isCondensed
    ? width * variables.panels.baseWidth
    : variables.panels.condensedBaseWidthOffset
  const sectionWidth = !isCondensed
    ? width * variables.panels.sectionWidth
    : variables.panels.condensedSectionWidth

  const topLeft = {
    x: 0,
    y: 0,
  }
  const bottomLeft = {
    x: 0,
    y: height,
  }
  const bottomRight = {
    x: baseWidth + number * sectionWidth,
    y: height,
  }
  const topRight = {
    x:
      baseWidth +
      (number * sectionWidth +
        Math.tan(((90 - angle) * Math.PI) / 180) * height),
    y: 0,
  }

  const polygonContactPoints = `${topLeft.x},${topLeft.y} ${bottomLeft.x},${bottomLeft.y} ${width},${bottomRight.y} ${width}, ${topRight.y}`
  const polygonPoints = `${topLeft.x},${topLeft.y} ${bottomLeft.x},${bottomLeft.y} ${bottomRight.x},${bottomRight.y} ${topRight.x},${topRight.y}`

  const polygonHoverPoints = `${topRight.x - sectionWidth},${
    topLeft.y
  } ${bottomRight.x - sectionWidth},${bottomLeft.y} ${bottomRight.x},${
    bottomRight.y
  } ${topRight.x},${topRight.y}`

  const polygonContactPointsCss = `${topLeft.x}px ${topLeft.y}px, ${bottomLeft.x}px ${bottomLeft.y}px, ${width}px ${bottomRight.y}px, ${width}px ${topRight.y}px`
  const polygonPointsCss = `${topLeft.x}px ${topLeft.y}px, ${bottomLeft.x}px ${bottomLeft.y}px, ${bottomRight.x}px ${bottomRight.y}px, ${topRight.x}px ${topRight.y}px`

  const polygonHoverPointsCss = `0 0, 0 100%, ${bottomRight.x}px ${bottomRight.y}px, ${topRight.x}px ${topRight.y}px`
  const polygonNull = `0,0 0,0 0,0 0,0`

  const handleHoverPoints = () => {
    if (number !== 0) return polygonHoverPoints
    else return polygonPoints
  }

  const handleHoverPointsCss = () => {
    if (number !== 0) return polygonHoverPointsCss
    else if (number !== totalSections - 1) return polygonContactPointsCss
    else return polygonPointsCss
  }

  const handlePoints = () => {
    if (number !== totalSections - 1) return polygonPoints
    else return polygonContactPoints
  }

  const handleMouseEnter = () => {
    if (isDragging) {
      setPreviewed(number)
      setActiveSection(null)
    }
  }

  const handleMouseLeave = () => {
    if (isDragging) setPreviewed(number)
  }

  const handleMouseUp = () => {
    if (isDragging) setActiveSection(number)
  }

  const handleOnClick = () => {
    if (!isDragging) {
      if (previewed !== number) {
        setPreviewed(number)
        setActiveSection(number)
      } else setActiveSection(number)
    }
  }

  const handleZIndex = () => {
    if (isDragging && previewed === number) return true
  }

  const backgroundFill = () => {
    if (activeSection === totalSections - 1) return `none`
    else if (activeSection === number) return `white`
    else return color
  }

  const cssBackgroundFill = () => {
    if (activeSection === number) {
      return false
    } else return true
  }

  const style = {
    wrapper: {
      position: "fixed",
      top: "0",
      left: "0",
      pointerEvents: "none",
      zIndex: handleZIndex()
        ? 2 * totalSections - number
        : totalSections - number,
    },
    overlay: {
      clipPath: `polygon(${handleHoverPointsCss()})`,
      backgroundImage: section.hero_image
        ? `linear-gradient( ${variables.color.purple}, ${variables.color.blue})`
        : `transparent`,
      backgroundSize: "cover",
      position: "fixed",
      display: "block",
      width: "100%",
      height: "100%",
      top: 0,
      left: 0,
      zIndex: 40,
      opacity: cssBackgroundFill() ? "0.5" : "0",
      transition: "all 0.8s ease",
    },
    bgImage: {
      clipPath: `polygon(${handleHoverPointsCss()})`,
      backgroundImage: section.hero_image
        ? `url(${section.hero_image.url})`
        : `transparent`,
      backgroundSize: "cover",
      position: "fixed",
      display: "block",
      width: "100%",
      height: "100%",
      top: 0,
      left: 0,
      zIndex: 40,
      opacity: cssBackgroundFill() ? "1" : "0",
      transition: "all 0.8s ease",
    },
  }

  return (
    <div style={style.wrapper}>
      <div className="bgImage" style={style.bgImage} />
      <div className="overlay" style={style.overlay} />
      <svg
        viewBox={`0 0 ${width} ${height}`}
        width={width}
        hight={height}
        pointerEvents="none"
      >
        <polygon
          onMouseEnter={() => handleMouseEnter()}
          onMouseOut={() => handleMouseLeave()}
          onMouseUp={() => handleMouseUp()}
          onClick={() => handleOnClick()}
          pointerEvents="fill"
          points={handleHoverPoints()}
          fill="none"
          style={{ cursor: "pointer" }}
        />
        <polygon
          pointerEvents="none"
          points={handlePoints()}
          fill={number === totalSections - 1 ? `none` : backgroundFill()}
        />
      </svg>
    </div>
  )
}

export default Bg
