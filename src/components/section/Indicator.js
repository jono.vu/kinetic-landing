import React, { useState } from "react"

import HomeIcon from "../../img/home.svg"
import ContactIcon from "../../img/contact.svg"

import { variables } from "../../utils/variables"
import { useWindowDimensions } from "../../utils/useWindowDimensions"

import styles from "./css/indicator.module.scss"

const Indicator = props => {
  const { width } = useWindowDimensions()

  const number = props.number
  const previewed = props.previewed

  const isDragging = props.isDragging

  const setPreviewed = props.setPreviewed

  const activeSection = props.activeSection
  const setActiveSection = props.setActiveSection

  const [hover, setHover] = useState(false)

  const indicatorStyle = () => {
    const defaultLeft = (width * variables.panels.sectionWidth) / 2
    const defaultTop = 40
    if (activeSection === number)
      return {
        backgroundColor: variables.color.greenTransparent,
        borderColor: variables.color.green,
        width: 20,
        height: 20,
        defaultLeft: defaultLeft,
        left: defaultLeft + 3,
        defaultTop: defaultTop,
        top: defaultTop,
        opacity: "1",
      }
    else if (previewed === number)
      return {
        backgroundColor: variables.color.greenTransparent,
        borderColor: variables.color.green,
        width: 20,
        height: 20,
        defaultLeft: defaultLeft,
        left: defaultLeft + 3,
        defaultTop: defaultTop,
        top: defaultTop,
        opacity: "1",
      }
    else if (hover) {
      return {
        backgroundColor: variables.color.greenTransparent,
        borderColor: variables.color.green,
        width: 20,
        height: 20,
        defaultLeft: defaultLeft,
        left: defaultLeft + 3,
        defaultTop: defaultTop,
        top: defaultTop,
        opacity: "0.4",
      }
    } else
      return {
        backgroundColor: variables.color.greenTransparent,
        borderColor: variables.color.green,
        width: 10,
        height: 10,
        defaultLeft: defaultLeft,
        left: defaultLeft + 8,
        defaultTop: defaultTop,
        top: defaultTop + 5,
        opacity: "0.4",
      }
  }

  const icon = () => {
    if (number === 0) return HomeIcon
    else if (number === 4) return ContactIcon
  }

  const indicatorOffset =
    number === 0
      ? -(width * variables.panels.sectionWidth) / 2 + 13 + 40
      : width * variables.panels.baseWidth +
        (number - 1) * width * variables.panels.sectionWidth

  const desktop = {
    interactive: {
      display: "inlineBlock",
      width: width * variables.panels.sectionWidth,
      height: 100,
      position: "fixed",
      zIndex: 21,
      bottom: "0",
      borderRadius: 200,
      left: indicatorOffset,
      cursor: "pointer",
    },
    icon: {
      display: "inlineBlock",
      backgroundImage: `url(${icon()}`,
      backgroundSize: `contain`,
      backgroundRepeat: `no-repeat`,
      width: 30,
      height: 30,
      position: "absolute",
      left: indicatorStyle().defaultLeft,
      top: indicatorStyle().defaultTop,
      zIndex: 22,
      pointerEvents: "none",
      transition: "all 0.3s ease",
    },
    indicator: {
      display: "inlineBlock", //icon() ? "none" : "inlineBlock",
      backgroundColor: indicatorStyle().backgroundColor,
      border: "1px solid transparent",
      borderColor: indicatorStyle().borderColor,
      width: indicatorStyle().width,
      height: indicatorStyle().height,
      opacity: indicatorStyle().opacity,
      zIndex: 21,
      borderRadius: 40,
      left: indicatorStyle().left,
      position: "relative",
      top: indicatorStyle().top,
      pointerEvents: "none",
      transition: "all 0.1s ease",
    },
  }

  const handleMouseEnter = () => {
    if (isDragging) {
      setPreviewed(number)
      setActiveSection(null)
    } else if (!isDragging) {
      setHover(true)
    }
  }

  const handleMouseLeave = () => {
    if (isDragging) setPreviewed(number)
    else if (!isDragging) {
      setHover(false)
    }
  }

  const handleMouseUp = () => {
    if (isDragging) setActiveSection(number)
  }

  const handleOnClick = () => {
    if (!isDragging) {
      setPreviewed(number)
      setActiveSection(number)
    }
  }

  return (
    <div
      style={desktop.interactive}
      className={styles.interactive}
      onMouseEnter={() => handleMouseEnter()}
      onMouseOut={() => handleMouseLeave()}
      onMouseUp={() => handleMouseUp()}
      onClick={() => handleOnClick()}
    >
      <div style={desktop.icon} className={styles.icon} />
      <div style={!icon() ? desktop.indicator : {}} />
    </div>
  )
}

export default Indicator
