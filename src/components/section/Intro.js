import React from "react"

import Title from "../ui/Title"
import BreadCrumb from "../ui/BreadCrumb"
import Text from "../ui/Text"

const Intro = props => {
  const section = props.section
  const title = section.title[0].text
  const description = section.intro.description[0].text
  const punchline = section.intro.punchline && section.intro.punchline[0].text

  const activeOffset = props.activeOffset

  const pointerEventsNone = props.pointerEvents

  return (
    <div
      style={{
        zIndex: 11,
        position: "relative",
        transform: `translateX(${activeOffset}px)`,
        transition: "transform 0.8s ease",
        pointerEvents: pointerEventsNone,
      }}
    >
      <BreadCrumb {...props} />
      <Title {...props}>{title}</Title>
      <Text {...props}>{description}</Text>
      <Text {...props}>{punchline}</Text>
    </div>
  )
}

export default Intro
