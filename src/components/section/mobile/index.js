import React from "react"

import Surface from "./Surface"
import Content from "../content/index.js"

const MobileSection = props => {
  const { activeSection, number } = props

  const currentSection = activeSection === number

  const displayIntro = () => {
    if (activeSection === 0) return true
    else if (number === activeSection) return true
    // current module displays title
    else if (number === activeSection + 1) return true
    // next module button
    else if (number === activeSection - 1) return true
    // previous module button
    else return false
  }
  console.log(activeSection, number, displayIntro())

  return (
    <div style={{ minHeight: currentSection && "100vh", position: "relative" }}>
      {displayIntro() && <Surface {...props} />}
      {currentSection && <Content {...props} />}
    </div>
  )
}

export default MobileSection
