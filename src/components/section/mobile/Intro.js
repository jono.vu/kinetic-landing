import React from "react"

import Title from "../../ui/Title"
import BreadCrumb from "../../ui/mobile/BreadCrumb"
import Text from "../../ui/mobile/Text"

const Intro = props => {
  const { section, activeSection, number } = props
  const title = section.title[0].text
  const description = section.intro.description[0].text
  const punchline = section.intro.punchline && section.intro.punchline[0].text

  return (
    <div>
      <BreadCrumb {...props} />
      {activeSection === number && (
        <>
          <Title {...props}>{title}</Title>
          <Text {...props}>{description}</Text>
          <Text {...props}>{punchline}</Text>
        </>
      )}
    </div>
  )
}

export default Intro
