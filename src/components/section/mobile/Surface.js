import React from "react"

import Intro from "./Intro"
import Bg from "./Bg"

const Surface = props => {
  const { setActiveSection, number } = props
  return (
    <div
      onClick={() => {
        setActiveSection(number)
        window.scrollTo(0, 0)
      }}
    >
      <Intro {...props} />
      <Bg {...props} />
    </div>
  )
}

export default Surface
