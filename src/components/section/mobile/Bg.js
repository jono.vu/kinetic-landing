import React from "react"

const Bg = props => {
  const { section, number, color, totalSections, activeSection } = props
  const footerHeight = 70
  const image = () => {
    if (number === 0) return `white`
    else if (number === totalSections - 1) return `green`
    else if (section.hero_image) return `url("${section.hero_image.url}")`
  }
  const overlay = () => {
    if (number === 0) return `transparent`
    else if (number === activeSection) return `transparent`
    return `blue`
  }
  const bgHeight = () => {
    if (number === 0) return `calc(100vh - ${footerHeight}px)`
    else if (
      activeSection === totalSections - 1 &&
      number === totalSections - 1
    )
      return 0
    else if (number === activeSection) return "200px"
    else return "100%"
  }
  const absoluteBgColor = () => {
    if (activeSection === 0) return `transparent`
    else if (activeSection === totalSections - 1) return `transparent`
    else return `white`
  }

  const position = () => {
    if (number === 0) return `absolute`
    else if (number === activeSection) return `relative`
    else return `absolute`
  }
  return (
    <>
      <div
        style={{
          position: `absolute`,
          zIndex: -1,
          top: 0,
          width: "100vw",
          opacity: 0.6,
          height: bgHeight(),
          background: overlay(),
        }}
      />
      <div
        style={{
          position: position(),
          zIndex: -2,
          top: 0,
          width: "100vw",
          height: bgHeight(),
          background: image(),
          backgroundSize: "cover",
        }}
      />
      <div
        className="absolute-background"
        style={{
          zIndex: -3,
          top: 0,
          width: "100vw",
          height: "100%",
          backgroundColor: absoluteBgColor(),
          position: `fixed`,
        }}
      />
    </>
  )
}

export default Bg
