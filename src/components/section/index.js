import React, { useState, useEffect } from "react"

import { variables } from "../../utils/variables"

import Surface from "./Surface"
import Content from "./content/index.js"

const Section = props => {
  const number = props.number
  const previewed = props.previewed
  const activeSection = props.activeSection
  const isDragging = props.isDragging

  const isActiveOrPreviewed = () => {
    if (!isDragging && activeSection === null && previewed === number)
      return true
    else if (activeSection === number) return true
  }

  const activeOffset =
    isActiveOrPreviewed() && number * variables.panels.condensedSectionWidth

  const [pointerEventsNone, setPointerEventsNone] = useState(`none`)

  useEffect(() => {
    if (isDragging) setPointerEventsNone(`none`)
    else if (activeSection !== number) setPointerEventsNone(`none`)
    else setPointerEventsNone(`auto`)
  }, [isDragging])

  return (
    <div style={{ position: "relative" }}>
      <Surface
        {...props}
        activeOffset={activeOffset}
        pointerEventsNone={pointerEventsNone}
      />
      {activeSection === number && previewed === number && (
        <Content
          {...props}
          activeOffset={activeOffset}
          pointerEventsNone={pointerEventsNone}
        />
      )}
    </div>
  )
}

export default Section
