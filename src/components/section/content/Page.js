import React from "react"

import Paragraphs from "./Paragraphs"
import Socials from "./Socials"

const Page = props => {
  const { isMobile } = props
  return (
    <div
      style={{
        zIndex: 11,
        marginBottom: !isMobile && 300,
        pointerEvents: props.pointerEvents,
      }}
    >
      <Paragraphs {...props} />
      <Socials {...props} />
    </div>
  )
}

export default Page
