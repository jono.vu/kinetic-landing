import React from "react"

import Text from "../../ui/Text"
import MobileText from "../../ui/mobile/Text"

const Paragraph = props => {
  const { text, isMobile } = props

  return !isMobile ? (
    <Text {...props}>{text}</Text>
  ) : (
    <MobileText {...props}>{text}</MobileText>
  )
}

const Diagram = props => {
  const desktop = {
    image: {
      position: "absolute",
      maxWidth: 400,
      marginBottom: 30,
      left: 600,
    },
  }
  return <img style={desktop.image} src={props.src} />
}

const MobileDiagram = props => {
  const mobile = {
    image: {
      width: `calc(100vw - 40px)`,
      position: `relative`,
      display: `block`,
      margin: `auto`,
    },
  }
  return <img style={mobile.image} src={props.src} />
}

const Paragraphs = props => {
  const { section, isMobile } = props
  const fields = section.content || []
  return (
    <div
      style={{
        display: "inlineBlock",
        maxWidth: "500px",
      }}
    >
      {fields.map((field, i) => {
        const url = field.image && field.image.url
        return (
          <>
            <Paragraph key={i} text={field.paragraph[0].text} {...props} />
            {!isMobile ? (
              <Diagram key={i} src={url} {...props} />
            ) : (
              <MobileDiagram key={i} src={url} {...props} />
            )}
          </>
        )
      })}
    </div>
  )
}

export default Paragraphs
