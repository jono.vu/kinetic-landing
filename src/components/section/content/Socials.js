import React from "react"

import Button from "../../ui/Button"

const Social = props => {
  const { social } = props
  const desktop = {
    img: { width: "30px", marginRight: "30px" },
  }
  return (
    <a href={social.link.url} target="_blank">
      <img style={desktop.img} src={social.icon.url} />
    </a>
  )
}

const Socials = props => {
  const { isMobile } = props
  const contact = props.section.contact
  const socials = contact && contact.socials
  const desktop = {
    title: {
      marginBottom: 20,
      borderBottom: "1px solid white",
      marginLeft: isMobile ? 25 : 50,
      color: "white",
    },
    wrapper: {
      display: "flex",
      alignItems: "center",
      marginLeft: isMobile ? 25 : 50,
      marginBottom: 20,
    },
    button: {
      marginLeft: isMobile ? 25 : 50,
      marginBottom: 20,
      cursor: "pointer",
    },
    rights: {
      marginLeft: isMobile ? 25 : 50,
      color: "white",
      marginBottom: 50,
    },
  }

  return (
    <>
      {socials && (
        <>
          <h3 style={desktop.title}>{contact.title}:</h3>
          <wrapper style={desktop.wrapper}>
            {socials.map(social => {
              return <Social social={social} />
            })}
          </wrapper>
          <>
            <a href="https://momentium.com.au" target="_blank">
              <Button style={desktop.button}>{contact.button}</Button>
            </a>
            <p style={desktop.rights}>{contact.rights}</p>
          </>
        </>
      )}
    </>
  )
}

export default Socials
