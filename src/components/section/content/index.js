import React from "react"

import Page from "./Page"

const Content = props => {
  const activeOffset = props.activeOffset

  return (
    <div
      style={{
        zIndex: 11,
        position: "relative",
        left: activeOffset,
        display: "flex",
      }}
    >
      <Page {...props} />
    </div>
  )
}

export default Content
