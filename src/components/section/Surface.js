import React from "react"

import Intro from "./Intro"
import Bg from "./Bg"
import Indicator from "./Indicator"

const Surface = props => {
  const section = props.section
  const number = props.number
  const isDragging = props.isDragging
  const previewed = props.previewed
  const activeSection = props.activeSection
  const totalSections = props.totalSections

  const introDisplay = () => previewed === number
  const isCondensed = () => {
    if (activeSection === 0 && !isDragging) return false
    else if (isDragging) return false
    else if (previewed === number) return false
    else if (previewed !== number) return true
    else if (activeSection === 0) return true
    else if (activeSection === number) return true
    else if (number === totalSections - 1) return true
  }
  return (
    <>
      <Indicator {...props} isCondensed={isCondensed()} />
      {introDisplay() && <Intro {...props} isCondensed={isCondensed()} />}
      <Bg {...props} isCondensed={isCondensed()} />
    </>
  )
}

export default Surface
