import React from "react"

import Section from "./section/index.js"
import MobileSection from "./section/mobile/index.js"

import { useSectionsData } from "../utils/data"

const Arrows = () => {
  return <div style={{ position: "absolute" }}>{`< & >`}</div>
}

const Sections = props => {
  const { isDragging, isMobile } = props
  const sections = useSectionsData()

  return (
    <div style={{ pointerEvents: isDragging && "none" }}>
      {sections.map(section => {
        return (
          <>
            {!isMobile ? (
              <Section
                key={section.number}
                number={section.number}
                section={section}
                totalSections={sections.length}
                isDragging={isDragging}
                {...props}
              />
            ) : (
              <MobileSection
                key={section.number}
                number={section.number}
                section={section}
                totalSections={sections.length}
                {...props}
              />
            )}
          </>
        )
      })}
    </div>
  )
}

const Interactive = props => {
  const isDragging = props.isDragging
  const setIsDragging = props.setIsDragging
  const handleMouseMove = props.handleMouseMove

  const desktop = {
    interactive: {
      position: "absolute",
      left: "0",
      top: "0",
      width: "100%",
      height: "100%",
      cursor: isDragging && "grabbing",
    },
  }

  return (
    <div
      style={desktop.interactive}
      onMouseMove={e => {
        handleMouseMove(e)
      }}
      onMouseUp={e => {
        handleMouseMove(e)
        setIsDragging(false)
      }}
    >
      {props.children}
    </div>
  )
}

const Surface = props => {
  const { isDragging, isMobile } = props
  return (
    <>
      {!isMobile ? (
        <Interactive {...props}>
          <Arrows />
          <Sections isDragging={isDragging} {...props} />
        </Interactive>
      ) : (
        <Sections {...props} />
      )}
    </>
  )
}

export default Surface
