import React from "react"

const Title = props => {
  const { number, activeSection, totalSections } = props

  return <h2 {...props}>{props.children}</h2>
}

export default Title
