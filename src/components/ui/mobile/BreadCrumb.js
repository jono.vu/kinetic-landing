import React from "react"

import styles from "../css/breadcrumb.module.scss"

const BreadCrumb = props => {
  const { activeSection, number, totalSections, section } = props

  const color = () => {
    if (number === 0) return `inherit`
    else if (number === totalSections - 1) return `white`
    else if (number === activeSection) return `inherit`
    else return `white`
  }

  const borderColor = () => {
    if (activeSection === number) return `inherit`
    else return `transparent`
  }

  const text = `${number}. ${section.condensed_title}`

  return (
    <div
      className={styles.breadcrumb}
      style={{ color: color(), borderColor: borderColor() }}
    >
      {text}
    </div>
  )
}

export default BreadCrumb
