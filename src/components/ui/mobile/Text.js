import React from "react"

import styles from "../css/text.module.scss"

const Text = props => {
  const { number, activeSection, totalSections } = props

  return (
    <div
      {...props}
      className={styles.text}
      style={{ color: activeSection === totalSections - 1 && `white` }}
    >
      {props.children}
    </div>
  )
}

export default Text
