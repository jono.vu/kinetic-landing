import React from "react"

import styles from "./css/text.module.scss"
// import "./css/wow.scss"

const Text = props => {
  // const WOW = require("wowjs")
  // if (typeof window !== `undefined`) {
  //   window.wow = new WOW.WOW({
  //     live: false,
  //   })
  //   window.wow.init()
  // }

  const isDragging = props.isDragging

  const activeSection = props.activeSection
  const totalSections = props.totalSections

  const isWhite = () => {
    if (isDragging) return true
    else if (activeSection === totalSections - 1) return true
  }

  return (
    <div
      className={isWhite() && styles.isDragging}
      style={{ pointerEvents: isDragging && "none" }}
    >
      {/* <div className="wow fadeInDown" data-wow-offset="185"> */}
      <div {...props} className={styles.text}>
        {props.children}
        {/* </div> */}
      </div>
    </div>
  )
}

export default Text
