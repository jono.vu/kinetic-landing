import React from "react"

import styles from "./css/title.module.scss"

const Title = props => {
  const isDragging = props.isDragging

  const activeSection = props.activeSection
  const totalSections = props.totalSections

  const isWhite = () => {
    if (isDragging) return true
    else if (activeSection === totalSections - 1) return true
  }

  return (
    <div
      className={isWhite() && styles.isDragging}
      style={{ pointerEvents: isDragging && "none" }}
    >
      <h2 {...props} className={styles.title}>
        {props.children}
      </h2>
    </div>
  )
}

export default Title
