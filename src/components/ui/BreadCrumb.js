import React from "react"

import styles from "./css/breadcrumb.module.scss"

const BreadCrumb = props => {
  const isDragging = props.isDragging
  const previewed = props.setPreviewed

  const activeSection = props.activeSection
  const totalSections = props.totalSections

  const number = props.number
  const section = props.section

  const color = () => {
    if (activeSection === 0) return `inherit`
    else if (activeSection === totalSections - 1) return `white`
    else if (activeSection !== null) return props.section.color
  }

  const text = `${number}. ${section.condensed_title}`

  const isWhite = () => {
    if (isDragging) return true
    else if (previewed === number) return true
  }

  return (
    <div
      className={isWhite() && styles.isDragging}
      style={{ pointerEvents: isDragging && "none" }}
    >
      <div
        className={styles.breadcrumb}
        style={{ color: color(), borderColor: color() }}
      >
        {text}
      </div>
    </div>
  )
}

export default BreadCrumb
