import React from "react"

import styles from "./css/button.module.scss"

const Button = props => {
  return (
    <button style={props.style} className={styles.button}>
      {props.children}
    </button>
  )
}

export default Button
