import React from "react"

import Logo from "./Logo"

const Header = () => {
  return (
    <header style={{ position: "absolute" }}>
      <Logo />
    </header>
  )
}

export default Header
