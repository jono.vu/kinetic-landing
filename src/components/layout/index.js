import React from "react"

import Header from "./Header"
import Footer from "./Footer"

import styles from "./css/children.module.scss"

const Children = props => {
  return (
    <>
      {props.children}
      <div className={styles.bg} />
    </>
  )
}

const Layout = props => {
  const { isMobile } = props
  return (
    <>
      {!isMobile && <Header {...props} />}
      <Children>{props.children}</Children>
      {!isMobile && <Footer {...props} />}
    </>
  )
}

export default Layout
