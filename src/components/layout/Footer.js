import React from "react"

import { useSectionsData } from "../../utils/data.js"

const Footer = props => {
  const activeSection = props.activeSection
  const isDragging = props.isDragging

  const sections = useSectionsData()

  const color = () => {
    if (activeSection === sections.length - 1) return `transparent`
    else if (sections[activeSection] !== undefined)
      return sections[activeSection].color
    else return `transparent`
  }

  const footerVisible = () => {
    if (isDragging) return false
    else if (activeSection !== 0) return true
    else if (activeSection !== null) return false
  }

  const desktop = {
    footer: footerVisible()
      ? {
          height: 100,
          position: "fixed",
          zIndex: 20,
          width: "100%",
          bottom: 0,
          backgroundColor: color(),
        }
      : null,
  }
  return <footer style={desktop.footer} />
}

export default Footer
