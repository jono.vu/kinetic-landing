import React, { useState } from "react"

import MomentiumLogo from "../img/momentium.svg"

import { useSectionsData } from "../utils/data"

const MobileMenu = props => {
  const { setActiveSection } = props
  const sections = useSectionsData()
  const [isVisible, setVisible] = useState(false)
  return (
    <div
      style={{
        position: "fixed",
        zIndex: 40,
        top: 0,
        height: isVisible && "100vh",
        width: isVisible && "100vw",
        background: isVisible && `green`,
        padding: 40,
      }}
    >
      <div
        style={{
          right: 30,
          bottom: 30,
          position: `fixed`,
          border: `2px solid transparent`,
          borderColor: `green`,
          background: `green`,
          borderRadius: 80,
          width: 80,
          height: 80,
          zIndex: 30,
        }}
      >
        <img
          src={MomentiumLogo}
          style={{ width: 40, top: 17, left: 21, position: `absolute` }}
          onClick={() => {
            if (!isVisible) setVisible(true)
            else setVisible(false)
          }}
        />
      </div>
      {isVisible &&
        sections.map(section => {
          return (
            <div
              key={section.number}
              onClick={() => {
                setActiveSection(section.number)
                setVisible(false)
              }}
              style={{ color: `white`, lineHeight: `3` }}
            >
              {section.condensed_title}
            </div>
          )
        })}
    </div>
  )
}

export default MobileMenu
