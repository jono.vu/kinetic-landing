import React from "react"

import MomentiumLogo from "../img/momentium.svg"

import { variables } from "../utils/variables.js"

const Navigator = props => {
  const isDragging = props.isDragging

  const setIsDragging = props.setIsDragging
  const navigatorTransform = props.navigatorTransform

  const desktop = {
    navigator: {
      display: "inlineBlock",
      left: "0",
      top: "0",
      transform: `translate(${navigatorTransform.x -
        40}px, ${navigatorTransform.y - 40}px)`,
      position: "absolute",
      border: `2px solid transparent`,
      borderColor: isDragging ? variables.color.green : variables.color.green,
      background: isDragging
        ? variables.color.green
        : variables.color.greenTransparent,
      borderRadius: 80,
      width: 80,
      height: 80,
      zIndex: 30,
      pointerEvents: isDragging && "none",
      cursor: "grab",
    },
  }

  return (
    <div onMouseDown={() => setIsDragging(true)} style={desktop.navigator}>
      <img
        src={MomentiumLogo}
        style={{ width: 40, left: 21, top: 17, position: `absolute` }}
      />
    </div>
  )
}

export default Navigator
