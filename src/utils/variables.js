export const variables = {
  panels: {
    angle: 75, // angle of sections in degrees
    baseWidth: 1 / 3, // width of first section
    sectionWidth: 1 / 7, // width of section / width of screen
    condensedBaseWidthOffset: -120, // in px
    condensedSectionWidth: 100, // in px
  },
  color: {
    green: `#5BBC64`,
    greenTransparent: `#03CB6690`,
    greenDark: `#0CB665`,
    black: `#1C1B38`,
    purple: `#4466AF`,
    orange: `#EF4262`,
    yellow: `#F4CF1B`,
    blue: `#1C1B38`,
    cyan: `#1ABDC4`,
  },
}

// new green #18E17C new purp #6166D6
