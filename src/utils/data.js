import { useStaticQuery, graphql } from "gatsby"
import { variables } from "./variables"

import _ from "lodash"

export const useSectionsData = () => {
  const res = useStaticQuery(graphql`
    {
      prismic {
        allModules(sortBy: hierarchy_ASC) {
          edges {
            node {
              color
              hero_image
              hierarchy
              _meta {
                uid
              }
              body {
                ... on PRISMIC_ModuleBodyIntro {
                  type
                  primary {
                    description
                    title
                    condensed_title
                    punchline
                  }
                  label
                }
                ... on PRISMIC_ModuleBodyContent {
                  type
                  fields {
                    image
                    paragraph
                  }
                }
                ... on PRISMIC_ModuleBodySocial_links {
                  type
                  primary {
                    title
                    rights
                    button
                  }
                  fields {
                    icon
                    link {
                      ... on PRISMIC__ExternalLink {
                        url
                      }
                    }
                    service
                  }
                }
              }
            }
          }
        }
      }
    }
  `)
  const rawSections = res.prismic.allModules.edges
  const sections = rawSections.map(rawSection => {
    const section = rawSection.node
    const body = section.body

    const intro = _.find(body, { type: `intro` }) || {}
    const content = _.find(body, { type: `content` }) || {}
    const contact = _.find(body, { type: `social_links` }) || {}

    return {
      id: section._meta.uid,
      number: section.hierarchy,
      title: intro.primary.title,
      condensed_title: intro.primary.condensed_title,
      color: variables.color.purple, //section.color
      hero_image: section.hero_image,
      intro: {
        description: intro.primary.description,
        punchline: intro.primary.punchline,
      },
      content: content.fields,
      contact: contact.primary && {
        socials: contact.fields,
        button: contact.primary.button[0].text,
        rights: contact.primary.rights[0].text,
        title: contact.primary.title[0].text,
      },
    }
  })
  return sections
}
