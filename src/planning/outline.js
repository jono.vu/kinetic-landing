// Low Fidelity

const Layout = () => (
  <>
    <Header />
    <Children />
    <Footer />
  </>
)

const Landing = () => (
  <Layout>
    <Navigator />
    <Surface />
  </Layout>
)

const Surface = () => (
  <>
    <Arrows />
    <Sections />
  </>
)

const Sections = () => {
  map => <Section />
}

const Section = () => (
  <>
    <Surface />
    <Content />
  </>
)

const Surface = () => (
  <>
    <Intro />
    <Bg />
  </>
)

const Content = () => (
  <>
    <Page />
    <Diagrams />
  </>
)
