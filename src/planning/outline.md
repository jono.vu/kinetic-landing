# Features

- Users : no
- Auth : no
- Node Backend : no

---

- Static Pages : yes
- Contact Forms : yes
- Transitions : yes
- CMS : yes

# Folder Outline:

`root

    > src

      > components

        > css
          - landing.module.scss
          - navigator.module.scss

        - Landing.js *
        - Surface.js
          # Arrows
          # Sections
        - Navigator.js


        > layout

          > css
            - header.module.scss
            - footer.module.scss

          - index.js (Layout.js) *
            # Children
          - Header.js
          - Footer.js
          - Logo.js

        > section

          > css
            - section.module.scss
            - surface.module.scss
            - title.module.scss
            - bg.module.scss

          - index.js (Section.js)
          - skeleton.js
          - Surface.js
          - Intro.js
          - Bg.js

          > content

            > css
              - content.module.scss
              - page.module.scss
              - paragraph.module.scss
              - diagrams.module.scss

            - index.js (Content.js) *
            - skeleton.js
            - Page.js
            - Paragraphs.js
              # Paragraph
            - Diagrams.js
              # Diagram

      > pages

        - index.js

      > utils

        - useNavigator.js

`

* Design Mockups:

  - Landing
  - Layout
  - Content

# Dependencies

- gatsby-plugin-sass
- node-sass
- react-svg ?
